import React, { Component } from 'react';

class Counter extends Component {
	state = {
		value: this.props.value
	};
	// constructor() {
	// 	super();
	// 	this.handleIncrement = this.handleIncrement.bind(this);
	// }

	handleIncrement = (product) => {
		// this.state.count++;
		console.log(product);
		this.setState({ value: this.state.value + 1 });
	};

	// doHandleIncrement=()=>{
	// 	this.handleIncrement({id:1});
	// }
	render() {
		console.log('props',this.props);
		return (
			<React.Fragment>
				<h4>{this.props.id}</h4>
				<span className={this.getBadgeClasses()}>{this.formatCount()}</span>
				<button 
					onClick={()=>this.handleIncrement()} className="btn btn-secondary btn-sm">
					Increment
				</button>
			</React.Fragment>
		);
	}

	getBadgeClasses() {
		let clasess = 'badge m-2 badge-';
		clasess += this.state.value === 0 ? 'warning' : 'primary';
		return clasess;
	}

	formatCount() {
		const { value } = this.state;
		return value === 0 ? 'zero' : value;
	}
	renderTags() {
		if (this.state.tags.length === 0) return <p> There are no Tags</p>;

		return <ul>{this.state.tags.map((tag) => <li key={tag}>{tag} </li>)}</ul>;
	}
	createTagMessage() {
		return this.state.tags.length === 0 && 'Please create a new Tag';
	}
}

export default Counter;
